FROM node:18-bullseye as base-builder
WORKDIR /home/appuser/my_app
COPY . .
RUN rm Dockerfile README.md developpement.md production.md nginx.conf.template

FROM base-builder as builder-prod
RUN npm install
RUN npm run build

FROM nginx:stable-alpine as prod
COPY --from=base-builder /home/appuser/my_app/entrypoint.sh /bin
RUN rm -rf /usr/share/nginx/html/*; \
    mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.old; \
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old; \
    chmod +x /bin/entrypoint.sh
ENV SUBNET_STUB_STATUS 10.42.0.0/16
COPY --from=builder-prod /home/appuser/my_app/dist /usr/share/nginx/html
COPY --from=base-builder /home/appuser/my_app/nginx-stub_status.conf.template /etc/nginx/conf.d/nginx-stub_status.conf.template
COPY --from=base-builder /home/appuser/my_app/nginx.conf /etc/nginx/nginx.conf
ENTRYPOINT ["entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]